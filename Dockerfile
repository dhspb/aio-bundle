FROM alpine:3.9


# System-wide

RUN \
  apk add -U --no-progress --no-cache openssh-client xorriso git jq curl unzip qemu-img make bind-tools


# Ansible + mitogen

RUN \
  apk add -U --no-progress --no-cache libbz2 expat libffi gdbm ncurses-terminfo-base ncurses-terminfo ncurses-libs readline sqlite-libs python3 py3-pip gcc python3-dev musl-dev libffi-dev openssl-dev && \
  CRYPTOGRAPHY_DONT_BUILD_RUST=1 pip3 install --no-cache-dir ansible==2.9.6 certifi==2021.5.30 cffi==1.14.6 charset-normalizer==2.0.6 cryptography==3.4.8 hvac==0.11.1 idna==3.2 importlib-resources==5.2.2 Jinja2==3.0.1 jmespath==0.10.0 MarkupSafe==2.0.1 netaddr==0.8.0 pbr==5.6.0 pycparser==2.20 PyYAML==5.4.1 requests==2.26.0 ruamel.yaml==0.17.16 ruamel.yaml.clib==0.2.6 urllib3==1.26.7 zipp==3.5.0 && \
  find /usr/lib/python* -name \*.pyc | xargs rm && \
  curl -sL https://files.pythonhosted.org/packages/source/m/mitogen/mitogen-0.2.8.tar.gz | tar xzvf - -C /opt/ && \
  mkdir -p /etc/ansible && \
  echo -e '[defaults]\nhost_key_checking=False\ndeprecation_warnings=False\n[connection]\npipelining=True\n[ssh_connection]\npipelining=True\n' > /etc/ansible/ansible.cfg

RUN \
  apk add -U openssl ca-certificates fuse unzip && \
  cd /tmp && \
  curl -O https://downloads.rclone.org/rclone-current-linux-amd64.zip && \
  unzip /tmp/rclone-*.zip && \
  mv /tmp/rclone-*-linux-*/rclone /usr/bin && \
  rm -r /tmp/rclone*

# Terraform

RUN \
  wget https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip && \
  unzip terraform_0.11.13_linux_amd64.zip && \
  rm terraform_0.11.13_linux_amd64.zip && \
  mv terraform /bin && \
  mkdir -p /root/.terraform.d/plugins


# Terraform plugins

RUN \
  apk add -U --no-progress --no-cache libvirt-client && \
  wget -O terraform-provider-libvirt "https://gitlab.com/egeneralov/terraform-provider-libvirt/-/jobs/artifacts/fix-1/raw/terraform-provider-libvirt?job=alpine" && \
  chmod +x terraform-provider-libvirt && \
  mv terraform-provider-libvirt /root/.terraform.d/plugins

RUN \
  mkdir -p /root/.terraform.d/plugins && \
  wget -O terraform-provisioner-ansible "https://github.com/radekg/terraform-provisioner-ansible/releases/download/v2.1.2/terraform-provisioner-ansible-linux-amd64_v2.1.2" && \
  chmod +x terraform-provisioner-ansible && \
  mv terraform-provisioner-ansible /root/.terraform.d/plugins

RUN \
  wget https://github.com/adammck/terraform-inventory/releases/download/v0.8/terraform-inventory_v0.8_linux_amd64.zip && \
  unzip terraform-inventory_v0.8_linux_amd64.zip && \
  rm terraform-inventory_v0.8_linux_amd64.zip && \
  mv terraform-inventory /bin/

# Docker

RUN \
  curl -LO https://download.docker.com/linux/static/stable/x86_64/docker-20.10.8.tgz && \
  tar xvf docker-20.10.8.tgz && \
  mv docker/docker /bin/docker && \
  rm -rf docker-20.10.8.tgz docker

# K8S

RUN \
  curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.3/bin/linux/amd64/kubectl && \
  chmod +x ./kubectl && \
  mv ./kubectl /bin/kubectl

RUN \
  curl -sL https://get.helm.sh/helm-v3.6.0-linux-amd64.tar.gz | tar xzvf - && \
  chmod +x linux-amd64/helm && \
  mv linux-amd64/helm /bin/ && \
  rm -rf linux-amd64

RUN \
  curl -sL https://github.com/garethr/kubeval/releases/download/v0.16.1/kubeval-linux-amd64.tar.gz | tar xzvf - -C /bin/

RUN \
  curl -sL https://github.com/yannh/kubeconform/releases/download/v0.5.0/kubeconform-linux-amd64.tar.gz | tar xzvf - -C /bin/

RUN \
  curl -sSLo - https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v3.8.7/kustomize_v3.8.7_linux_amd64.tar.gz | tar xzf - -C /usr/local/bin

RUN \
  curl -Lo kind https://github.com/kubernetes-sigs/kind/releases/download/v0.11.1/kind-linux-amd64 && \
  chmod +x kind && \
  mv kind /bin/

# Custom

RUN \
  URL=$(curl -s https://api.github.com/repos/egeneralov/get-latest-registry-image-tag/releases/latest | jq -r .assets[].browser_download_url | grep linux) && \
  wget ${URL} -O /bin/get-latest-registry-image-tag && \
  chmod +x /bin/get-latest-registry-image-tag

